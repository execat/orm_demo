require 'sqlite3'
require 'sequel'
require 'pry'
require 'faker'

DB = Sequel.sqlite('database.sqlite3')

binding.pry

def create_tables
  # create an items table
  DB.create_table :items do
    primary_key :id
    String :name, unique: true, null: false
    Float :price, null: false
    String :description
  end

  DB.create_table :users do
    primary_key :id
    String :name
    Date :date_of_birth, null: false
  end

  DB.create_table :user_feedbacks do
    primary_key :id
    foreign_key :item_id, :items
    foreign_key :user_id, :users
    String :comment
  end
end

create_tables unless DB.table_exists?(:items)

class Item < Sequel::Model
  one_to_many :user_feedbacks
end

class UserFeedback < Sequel::Model
  one_to_one :item
  one_to_one :user, primary_key: :user_id, key: :id
end

class User < Sequel::Model
  one_to_many :user_feedbacks
end

puts 'Show how inserts and creates work'
binding.pry

def insert_items
  # create a dataset from the items table
  items = DB[:items]
  100.times do
    description = Faker::Vehicle.vin
    name = "#{Faker::Vehicle.make_and_model} #{description}"
    price = Faker::Number.decimal(5, 2)
    items.insert(name: name, price: price, description: description)
  end
end

def insert_comments
  50.times do
    User.create(name: Faker::Name.name, date_of_birth: Faker::Date.birthday(18, 65))
  end

  100.times do
    item_id = rand(100) + 1
    user_id = rand(50) + 1
    comment = [
      Faker::Community.quotes,
      Faker::Coffee.notes,
      Faker::Company.catch_phrase,
      Faker::FamousLastWords.last_words,
      Faker::MichaelScott.quote,
    ].sample
    puts "Adding #{item_id}, #{user_id} :: #{comment}"
    UserFeedback.create(item_id: item_id, user_id: user_id, comment: comment)
    # feedbacks = DB[:feedbacks]
    # feedbacks.insert(item_id: item_id, name: name, comment: comment)
  end
end

insert_items if DB[:items].count.zero?
insert_comments if UserFeedback.count.zero?

binding.pry
